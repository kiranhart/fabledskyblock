package com.songoda.skyblock.permission.permissions.listening;

import com.songoda.skyblock.permission.ListeningPermission;

public class NaturalMobSpawningPermission extends ListeningPermission {

    public NaturalMobSpawningPermission() {
        super("NaturalMobSpawning");
    }

}
